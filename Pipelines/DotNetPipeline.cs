using GitLab_CI_DSL.Builder;
using GitLab_CI_DSL.Generator;
using GitLab_CI_DSL.metamodel.pipeline;

namespace Pipelines
{
    public class DotNetPipeline
    {
        private readonly IPipelineBuilder _builder;

        // Output path and name
        private const string FILENAME = "dotnet_pipeline.yml";
        private const string PATH = "/home/jvs/Documents";

        public DotNetPipeline()
        {
            _builder = new PipelineBuilder();
            var pipeline = CreatePipeline();
            new GitLabYmlGenerator().CreateGitlabCiConfig(FILENAME, PATH, pipeline);
        }

        private Pipeline CreatePipeline()
        {
            return _builder.
                Pipeline().
                    AbstractJob(".Clean").
                        Image("mcr.microsoft.com/dotnet/core/runtime").
                        Script("dotnet clean").
                    AbstractJob(".RunVars").
                        EnvVar("RunVars", "Pipelines/").
                    Stage("Build").
                        Job("Build").
                            Extends(".Clean").
                            Script("dotnet build").
                    Stage("Run").
                        Job("Run").
                            Extends(".Clean").
                            Extends(".RunVars").
                            Script("dotnet run $RunVars").
                Create();
        }
    }
}