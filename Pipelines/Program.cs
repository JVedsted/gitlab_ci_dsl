﻿using System;

namespace Pipelines
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("######## Dot Net Core ########");
            new DotNetPipeline();
            
            
            Console.WriteLine("######## Simple Maven ########");
            new MavenPipeline();

            Console.WriteLine("\n######## Complex Maven ########");
            //new MavenProjPipeline();
            
        }
    }
} 